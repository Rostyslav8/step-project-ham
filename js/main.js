
function services() {
  const tabs = document.querySelector('.services-tabs');
  tabs.addEventListener('click', (e) => {
    document.querySelector('.services-tabs-item.active').classList.remove('active')
    e.target.classList.add('active')
    document.querySelector('.tabs-content .active').classList.remove('active')
    const data = e.target.getAttribute('data-tab')
    console.log(data);
    document.querySelector(`.tabs-content [data-tab="${data}"]`).classList.add('active')
  });
}

services()

function amazingWork() {
  document.querySelector('.work-list').addEventListener('mousedown', (event) => {
    document.querySelector('.work-item.active').classList.remove('active')
    event.target.classList.add('active');
    const data = event.target.getAttribute('data-id')
    document.querySelectorAll('.work-blocks-item').forEach(elem => {
      if (elem.getAttribute('data-id') === data || data === 'all') {
        elem.classList.remove('hide')
      } else {
        elem.classList.add('hide')
      }
    });
  })

  const parent = document.querySelector('.work-blocks-row')
  window.addEventListener('DOMContentLoaded', () => {
    let res = ''
    works.forEach((elem, i) => {
      if (i < 12) {
        res += `
        <li class="work-blocks-item graphic" data-id="${elem.data}">
                <img src="${elem.src}" alt="graphic" class="img-graphic">
                <div class="work-blocks-item-details">
                  <div class="item-details-links">
                    <a href="#" class="item-link icon-chain"></a>
                    <a href="#" class="item-link"><span></span></a>
                  </div>
                  <div class="item-details-title">creative design</div>
                  <div class="item-details-subtitle">${elem.data}</div>
                </div>
              </li>
        `
      }
    });
    parent.innerHTML = res;
  }
  )

  document.querySelector('.btn-work').addEventListener('click', function () {
    this.style.display = 'none'
    let res = ''
    works.forEach((elem, i) => {
      if (i >= 12) {
        res += `
        <li class="work-blocks-item graphic" data-id="${elem.data}">
                <img src="${elem.src}" alt="graphic" class="img-graphic">
                <div class="work-blocks-item-details">
                  <div class="item-details-links">
                    <a href="#" class="item-link icon-chain"></a>
                    <a href="#" class="item-link"><span></span></a>
                  </div>
                  <div class="item-details-title">creative design</div>
                  <div class="item-details-subtitle">${elem.data}</div>
                </div>
              </li>
        `
      }
    });
    parent.innerHTML += res;
  })

}

amazingWork()

const works = [
  {
    src: './img/graphic-design/graphic-design1.jpg',
    data: 'Graphic Design',
  },
  {
    src: './img/graphic-design/graphic-design2.jpg',
    data: 'Graphic Design',
  },
  {
    src: './img/graphic-design/graphic-design3.jpg',
    data: 'Graphic Design',
  },
  {
    src: './img/web-design/web-design1.jpg',
    data: 'Web Design',
  },
  {
    src: './img/web-design/web-design2.jpg',
    data: 'Web Design',
  },
  {
    src: './img/web-design/web-design3.jpg',
    data: 'Web Design',
  },
  {
    src: './img/landing-page/landing-page1.jpg',
    data: 'Landing Pages',
  },
  {
    src: './img/landing-page/landing-page2.jpg',
    data: 'Landing Pages',
  },
  {
    src: './img/landing-page/landing-page3.jpg',
    data: 'Landing Pages',
  },
  {
    src: './img/wordpress/wordpress1.jpg',
    data: 'Wordpress',
  },
  {
    src: './img/wordpress/wordpress2.jpg',
    data: 'Wordpress',
  },
  {
    src: './img/wordpress/wordpress3.jpg',
    data: 'Wordpress',
  },
];

//----- slider ------


let currentSlide = 0;
const navigation = document.querySelectorAll('.slider-user-foto-small');
const slides = document.querySelectorAll('.slider-user-review');
const next = document.getElementById('arrowRight');
const previous = document.getElementById('arrowLeft');

for (let i = 0; i < navigation.length; i++) {
  navigation[i].onclick = function () {
    currentSlide = i;
    document.querySelector('.slider-user-review.container.active').classList.remove('active');
    document.querySelector('.slider-user-foto-small.active').classList.remove('active');
    navigation[currentSlide].classList.add('active');
    slides[currentSlide].classList.add('active');
  }
}

next.onclick = function () {
  nextSlide(currentSlide);
};

previous.onclick = function () {
  previousSlide(currentSlide);
};

function nextSlide() {
  goToSlide(currentSlide + 1);
}

function previousSlide() {
  goToSlide(currentSlide - 1);
}

function goToSlide(n) {
  hideSlides();
  currentSlide = (n + slides.length) % slides.length;
  showSlides();
}

function hideSlides() {
  slides[currentSlide].className = 'slider-user-review container';
  navigation[currentSlide].className = 'slider-user-foto-small';
}

function showSlides() {
  slides[currentSlide].className = 'slider-user-review container active';
  navigation[currentSlide].className = 'slider-user-foto-small active';
}



